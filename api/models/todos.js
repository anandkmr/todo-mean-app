const mongoose = require('mongoose');

const todoSchema = new mongoose.Schema({
  user_id: {
    type: Number,
    required: true
  },
  name: {
    type: String,
    required: true
  },
  completed: {
    type: Boolean,
    required: true
  }
});

mongoose.model('Todo', todoSchema);
