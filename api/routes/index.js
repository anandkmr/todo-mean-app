const ctrlAuth = require('../controllers/authentication');
const ctrlProfile = require('../controllers/profile');
const ctrlTodo = require('../controllers/todos');

const express = require('express');
const jwt = require('express-jwt');

const auth = jwt({
  secret: 'MY_SECRET',
  userProperty: 'payload',
  algorithms: ['HS256']
});

const router = express.Router();

// profile
router.get('/profile', auth, ctrlProfile.profileRead);

// authentication
router.post('/login', ctrlAuth.login);
router.post('/register', ctrlAuth.register);


module.exports = router;