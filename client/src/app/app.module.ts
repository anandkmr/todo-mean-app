import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { TodosComponent } from './todos/todos.component';
import { todosService } from './todos/todo.service'
import { HttpErrorHandler } from "./http-error-handler.service";
import { MessageService } from "./message.service";

// ALL COMPONENTS ARE DECLARED HERE,   IMPORTS ARE MENTIONTED AND    PROVIDERS ARE DEFINED. 
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ProfileComponent,
    TodosComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule, 
    FormsModule, 
    HttpClientModule
  ],
  providers: [todosService, HttpErrorHandler, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
