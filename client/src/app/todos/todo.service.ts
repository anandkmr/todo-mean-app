import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { HttpHeaders } from "@angular/common/http";

import { Observable } from "rxjs";
import { catchError } from "rxjs/operators";

import { Todo } from "./todo";
import { HttpErrorHandler, HandleError } from "../http-error-handler.service";

@Injectable({
  providedIn: 'root'
})
export class todosService {
  private handleError: HandleError;

  constructor(private http: HttpClient, httpErrorHandler: HttpErrorHandler) {
    this.handleError = httpErrorHandler.createHandleError("todosService");
  }

  // GET ALL THE TASKS
  getTasks(): Observable<Todo[]> {
    return this.http
      .get<Todo[]>("api/todos")
      .pipe(catchError(this.handleError("getTasks", [])));
  }

  //ADD NEW TASK
  addTask(todo: Todo): Observable<Todo> {
    return this.http
      .post<Todo>("api/todo", todo)
      .pipe(catchError(this.handleError("addTask", todo)));
  }

  //Delete A TASK
  deleteTask(id: number): Observable<{}> {
    const url = `api/todo/${id}`;
    return this.http
      .delete(url)
      .pipe(catchError(this.handleError("deleteTask")));
  }

  //UPDATE A TASK
  updateTask(todo: Todo): Observable<Todo> {
    return this.http
      .put<Todo>(`api/todos/${todo._id}`, todo)
      .pipe(catchError(this.handleError("updateTask", todo)));
  }
}