// Interface of Todo/Task 

export interface Todo {
    _id: number
    user_id: number
    name: String
    completed: boolean

  }