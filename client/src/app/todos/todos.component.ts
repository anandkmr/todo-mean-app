import { Component, OnInit } from '@angular/core';
import { todosService } from './todo.service';
import { Todo } from './todo';


@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

todos: Todo[];
editTask: Todo;

  constructor(private todosService: todosService) { }

  ngOnInit(): void {
    this.getTodos();
  }

  getTodos():void{
    this.todosService.getTasks().subscribe(todos => (this.todos = todos))
  }

    //ADD NEW TASK
  add(_id: number, user_id: number, name: string, completed: boolean): void {
    this.editTask = undefined
    name = name.trim()
    if (!name) {
      return
    }

    const newTodo: Todo = { _id, user_id, name, completed } as Todo
    this.todosService.addTask(newTodo).subscribe(todo => this.todos.push(todo))
  }

    //Delete A TASK
  delete(task: Todo): void {
    this.todos = this.todos.filter(h => h !== task)
    this.todosService.deleteTask(task._id).subscribe()
  }

 
  edit(task) {
    this.editTask = task
  }

  //UPDATE A TASK
  update() {
    if (this.editTask) {
      this.todosService.updateTask(this.editTask).subscribe(task => {
        const ix = task ? this.todos.findIndex(h => h._id === task._id) : -1
        if (ix > -1) {
          this.todos[ix] = task
        }
      })
      this.editTask = undefined
    }
  }

}
