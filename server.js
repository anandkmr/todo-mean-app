require('./api/models/db');

var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser');
var createError = require('http-errors');
var passport = require('passport');
var express = require('express');
var logger = require('morgan');
var path = require('path');
var mongoose = require('mongoose');



var routesApi = require('./api/routes/index');

var port = 3000

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));                 // logs every request to the console
app.use(express.json());
app.use(express.urlencoded({ extended: false }));    // parse application/x-www-form-urlencoded
app.use(cookieParser());

// Set Static Folder
app.use(express.static(path.join(__dirname, 'client')))

app.use(passport.initialize());
app.use('/api', routesApi);

const Todo = mongoose.model('Todo');
app.get("/api/todos", getAllTasks);
function getAllTasks(req, res){
  Todo.find({ user_id: req.body.user_id}).exec(function(err, todos) {
    res.status(200).json(todos);
  });
}

app.post("/api/todos", createTask);
function createTask(req, res){
  var todoTask = req.body;    
    //save the todoTask in db    
    Todo    
        .create(todoTask)    
        .then(    
            function (success) {    
                console.log('Success');    
            },    
            function (error) {    
                console.log('Error');    
            }    
        )    
        
    res.json(todoTask);
}

// catch 404 and forward to error handler
app.use((req, res, next) => {
  console.log("NOT FOUND ERROR SHOWN");
  next(createError(404));

});

// Catch unauthorised errors
app.use((err, req, res) => {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({ message: `${err.name}: ${err.message}` });
  }
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.listen(port, function () {
  console.log('Server started on port ' + port)
})